import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./Pages/HOC/Layout";
import LoginPage from "./Pages/Login-Register_Page/LoginPage";
import RegisterPage from "./Pages/Login-Register_Page/RegisterPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route path="/register" element={<Layout Component={RegisterPage} />} />

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
