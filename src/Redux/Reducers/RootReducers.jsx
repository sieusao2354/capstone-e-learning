import { combineReducers } from "redux";
import { CourseReducers } from "./CourseReducers";
import { UserReducers } from "./UserReducers";

export let RootReducers = combineReducers({
  CourseReducers,
  UserReducers,
});
