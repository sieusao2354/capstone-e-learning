import { CATEGORY_COURSE, ID_CATEGORY_COURSE, LIST_COURSE } from "../constants/getListCourses";

const initialState = {
  listCourses: [],
  categoryCourse: [],
  IdCourse: []

};

export let CourseReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case LIST_COURSE: {
      state.listCourses = payload;
      return { ...state };
    }
    case CATEGORY_COURSE: {
      state.categoryCourse = payload
      return { ...state }
    }
    case ID_CATEGORY_COURSE: {
      state.IdCourse = payload
      return { ...state }
    }
    default:
      return state;
  }
};
