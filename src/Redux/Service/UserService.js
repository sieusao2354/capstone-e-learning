import { https } from "./ConfigURL"

export let userService = {
    postLogin: (loginData) => {
        return https.post("/api/QuanLyNguoiDung/DangNhap", loginData)
    },
    postRegister: (loginRegister) => {
        return https.post("/api/QuanLyNguoiDung/DangKy", loginRegister)
    },
    
}