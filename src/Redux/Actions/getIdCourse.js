import { ID_CATEGORY_COURSE } from "../constants/getListCourses";
import { courseService } from "../Service/CourseService"

export let getIdCourse = () => {
    return async (dispatch) => {
        await courseService.getIdCourse()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: ID_CATEGORY_COURSE,
                    payload: res.data
                })
            })
            .catch((err) => {
                console.log(err);
            });
    }
}