import { CATEGORY_COURSE } from "../constants/getListCourses";
import { courseService } from "../Service/CourseService"

export let getCategoryCourse = () => {
    return async (dispatch) => {
        await courseService.getCategoryCourse()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: CATEGORY_COURSE,
                    payload: res.data
                })
            })
            .catch((err) => {
                console.log(err);
            });
    }
}