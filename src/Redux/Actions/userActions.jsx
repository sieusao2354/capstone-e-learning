import { LOGIN, REGISTER } from "../Reducers/UserReducers"

export let loginActions = (dataLogin) => {
    return {
        type: LOGIN,
        payload: dataLogin,
    };

};
export let registerActions = (dataRegister) => {
    return {
        type: REGISTER,
        payload: dataRegister
    };
};