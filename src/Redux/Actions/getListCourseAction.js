import { LIST_COURSE } from "../constants/getListCourses";
import { courseService } from "./../Service/CourseService";

export let getListCourseAction = () => {
  return async (dispatch) => {
    await courseService
      .getListCourse()
      .then((res) => {
        // console.log(res.data);
        dispatch({
          type: LIST_COURSE,
          payload: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};


