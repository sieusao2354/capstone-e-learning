import {
  CloseOutlined,
  MenuUnfoldOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import React from "react";
import { useState } from "react";

export default function HeaderNav() {
  const [search, setSearch] = useState("");
  const [dropdown, setDropDown] = useState(true);

  let handleSearch = (e) => {
    let value = e.target.value;
    setSearch(value);
  };

  let handleOnDrop = (e) => {
    setDropDown(e);
    console.log(dropdown);
  };

  return (
    <div className=" bg-blue-500 mx-auto">
      <div className="flex container mx-auto justify-between items-center text-white font-medium h-20 ">
        <div className="text-2xl mx-2">
          <img src="" alt="" />
          LOGO
        </div>
        <div>
          <ul className="flex text-lg ">
            <a
              className="text-red-500   hover:text-red-500 transition-opacity active:text-red-500 bg-black bg-opacity-20 rounded-2xl"
              href=""
            >
              <li className="px-2 text-center">TRANG CHỦ</li>
            </a>

            <a className="hover:text-red-500 transition-opacity" href="">
              <li className="px-2">TRANG CÁ NHÂN</li>
            </a>
            <a className="hover:text-red-500 transition-opacity" href="">
              <li className="px-2">KHÓA HỌC</li>
            </a>

            <a className="hover:text-red-500 transition-opacity" href="">
              <li className="px-2 text-center">HƯỚNG DẪN SỬ DỤNG</li>
            </a>
          </ul>
        </div>
        <div className=" text-white relative  ">
          <input
            placeholder="Tìm kiếm khóa học"
            style={{ border: "solid 2px white" }}
            className=" bg-transparent py-2 px-3 rounded-full  "
            value={search}
            onChange={(e) => {
              handleSearch(e);
            }}
            type="text"
          />

          <SearchOutlined className="absolute text-white text-xl font-medium top-1 right-3 " />
        </div>

        <div style={{ lineHeight: "0px" }}>
          {dropdown ? (
            <button
              className="text-3xl"
              onClick={() => {
                handleOnDrop(false);
              }}
            >
              <MenuUnfoldOutlined />
            </button>
          ) : (
            <div>
              <button
                className="text-3xl"
                onClick={() => {
                  handleOnDrop(true);
                }}
              >
                <CloseOutlined />
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
