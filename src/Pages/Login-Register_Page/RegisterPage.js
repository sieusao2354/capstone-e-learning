
import {
    Form,
    Input,
    message,
} from 'antd';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { registerActions } from '../../Redux/Actions/userActions';
import { userService } from '../../Redux/Service/UserService';
export default function RegisterPage() {

    let dispatch = useDispatch()
    let history = useNavigate()


    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log('Received values of form: ', values);

        userService.postRegister(values)
            .then((res) => {
                message.success("Đăng nhập thành công")

                console.log(res);
                dispatch(registerActions(res.data.content))
                history("/login")

            }, [1000])
            .catch((err) => {
                message.error("Đăng nhập thất con mẹ nó bại")

                console.log(err);
            });






    };


    return (
        <div className="bg-gray-100 h-screen w-screen  py-20">
            <div className='py-5 container mx-auto max-w-screen-sm rounded-lg bg-white' >

                <p className=' text-white text-3xl font-medium text-center text-black  '>Đăng ký thành viên</p>
                <br />
                <div className='  ' >
                    <Form className=''


                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 8
                            ,
                        }}

                        form={form}
                        name="register"
                        onFinish={onFinish}

                        scrollToFirstError
                    >


                        <Form.Item
                            className=''


                            label={<p className=''>Tài khoản</p>}
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản ! ',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item

                            name="matKhau"
                            label="Mật khẩu"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu ! ',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="matKhau"
                            label="Nhập lại mật khẩu"
                            dependencies={['matKhau']}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng xác nhận mật khẩu ! ',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('matKhau') === value) {
                                            return Promise.resolve();
                                        }

                                        return Promise.reject(new Error('Xác nhận mật khẩu chưa đúng ! '));
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item

                            name="maNhom"
                            label="mã nhóm"
                            rules={[

                                {
                                    required: true,
                                    message: 'Vui lòng nhập mã nhóm !',
                                },
                            ]}
                        >
                            <Input placeholder='Nhập mã nhóm từ 01-09' />
                        </Form.Item>

                        <Form.Item
                            name="hoTen"
                            label="Họ và tên"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập họ tên !',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="soDT"
                            label="Số điện thoại"
                            rules={[


                                {
                                    required: true,
                                    message: 'Vui lòng nhập số điện thoại',
                                    whitespace: true,
                                },


                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="email"
                            label="E-mail"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'Email chưa đúng định dạng.',
                                },
                                {
                                    required: true,
                                    message: 'Vui lòng nhập Email',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <div className='flex justify-center space-x-5'>
                            <button className='rounded px-5 py-2 text-white bg-red-500 hover:bg-red-600'> đăng ký</button>
                            <button type='reset' className='rounded px-5 py-2 text-white bg-red-500 hover:bg-red-600'> Nhập lại</button>


                        </div>



                    </Form>
                </div>
            </div>
        </div>
    )
}
