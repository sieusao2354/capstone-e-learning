import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../Redux/Service/UserService";
import { useDispatch } from "react-redux";
import { loginActions } from "../../Redux/Actions/userActions";
import { LocalStorageService } from "../../Redux/Service/LocalStorageService";
import { useNavigate } from "react-router-dom";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();

  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch(loginActions(res.data.content));
        LocalStorageService.user.get(res.data.content);
        setTimeout(() => {
          history("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-gray-100 h-screen w-screen ">
      <div className="bg-gray-100">
        < div className=" py-20   " >
          <div style={{ width: 800, height: 400 }} className="bg-white  mx-auto " >
            <div className=" py-20  rounded-2xl ">
              <div
                style={{ width: 800, height: 400 }}
                className="bg-white  mx-auto "
              >
                <div className="py-10">
                  <p className="text-center text-medium text-3xl py-2  text-blue-400">
                    ĐĂNG NHẬP HỆ THỐNG
                  </p>
                  <hr className="w-3/4 mx-auto  " />
                </div>
                <div>
                  <Form

                    name="basic"
                    labelCol={{
                      span: 8,
                    }}
                    wrapperCol={{
                      span: 10,
                    }}
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                  >
                    <Form.Item
                      label={<p className="font-medium text-black"> Tài Khoản</p>}
                      name="taiKhoan"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng nhập Tài khoản!",
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="Mật Khẩu"
                      name="matKhau"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng nhập mật khẩu!",
                        },
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                    <Form.Item
                      name="remember"
                      valuePropName="checked"
                      wrapperCol={{
                        offset: 8,
                        span: 16,
                      }}
                    >
                      <Checkbox>Nhớ tài khoản</Checkbox>
                      <a className="text-blue-600 px-24" href="">
                        Quên Mật khẩu?
                      </a>
                    </Form.Item>

                    <Form.Item
                      wrapperCol={{
                        offset: 8,
                        span: 16,
                      }}
                    >
                      <Button
                        style={{ width: 108, height: 40 }}
                        className="mx-52 my-2 rounded bg-blue-400 text-white hover:bg-blue-500 hover:text-white"
                        htmlType="submit"
                      >
                        ĐĂNG NHẬP
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>

  )
}
