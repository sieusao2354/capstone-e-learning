import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { getCategoryCourse } from '../../Redux/Actions/getCategoryCourse'
import { getIdCourse } from '../../Redux/Actions/getIdCourse'
import { Card } from 'antd';
export default function CategoryCourse() {

    const { Meta } = Card;

    const onChange = (key) => {
        console.log(key);
    };

    let dispatch = useDispatch()
    const [isOpen, setOpen] = useState(true)

    let { categoryCourse, idCategoryCourse } = useSelector((state) => {

        return state.CourseReducers
    })

    useEffect(() => {
        dispatch(getCategoryCourse())
    }, [])
    useEffect(() => {
        dispatch(getIdCourse())
    })

    let renderCategory = () => {
        return categoryCourse.map((item, index) => {
            return <div key={index} className='px-5'>
                <div className=' container mx-auto py-10 mx-5' >
                    <button className=' p-2 rounded rounded-lg' >
                        {item.tenDanhMuc}
                    </button>



                </div>

            </div>

        })
    }



    let handleOpen = () => {
        setOpen(false)
    }


    return (
        < div className=' mx-auto  container text-medium text-lg flex  ' >

            {renderCategory()}



        </div >
    )
}
