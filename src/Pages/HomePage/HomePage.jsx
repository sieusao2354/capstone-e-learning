import React from "react";
import CategoryCourse from "./CategoryCourse";
import ListCourse from "./ListCourse";

export default function HomePage() {
  return (
    <div>
      <img className="w-full h-screen bg-cover" src="./img/banner.jpg" alt="" />
      <div>
        <CategoryCourse />
        <ListCourse />
      </div>
    </div>
  );
}
