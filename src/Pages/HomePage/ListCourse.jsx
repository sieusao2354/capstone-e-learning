import { useEffect } from "react";
import { getListCourseAction } from "./../../Redux/Actions/getListCourseAction";
import { useDispatch, useSelector } from "react-redux";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

export default function ListCourse() {
  const { listCourses } = useSelector((course) => {
    return course.CourseReducers;
  });
  // console.log(listCourses);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListCourseAction());
  }, []);
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  let renderContent = () => {
    return listCourses.splice(0, 8).map((item, index) => {
      return (
        <div key={index} >
          <div className="mx-auto container max-w-sm hover:ring bg-white rounded-lg border border-gray-200 shadow-md relative hover:shadow-2xl hover:shadow-slate-500 hover:translate-y-2  transition-all">
            <a className="text-center" chref="#">
              <img
                className="rounded-t-lg h-60 w-full  overflow-hidden "
                src={item.hinhAnh}
                alt
              />
            </a>
            <div className="p-5 h-56">
              <a href="#">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white rounded-2xl">
                  {item.tenKhoaHoc}
                </h5>
              </a>
              <p className="mt-5 font-normal text-gray-700 dark:text-gray-400">
                {item.moTa.length > 50
                  ? item.moTa.slice(0, 50) + "..."
                  : item.moTa}
              </p>
              <a
                href="#"
                className="absolute bottom-8 left-5 inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-500 rounded-lg hover:bg-blue-800 hover:translate-x-3 transition hover:text-white "
              >
                Read more
                <svg
                  aria-hidden="true"
                  className="ml-2 -mr-1 w-4 h-4"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="container mx-auto">
      {/* <div>Khóa Học Nổi Bật</div> */}
      <div className="  grid grid-cols-4 gap-5 py-20"> {renderContent()}</div>
    </div>
  );
}
