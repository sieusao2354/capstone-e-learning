import axios from "axios";

export const TOKEN_CYBER = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjE5LzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njc2NDgwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc2OTEyNDAwfQ.2Pn1sQiOcYDhAQ2DqfnG78MdznvbWOk0pOmrJLVW9hs";

export let https = axios.create({

    baseURL: "https://elearningnew.cybersoft.edu.vn",
    headers: {
        TokenCybersoft: TOKEN_CYBER,
    }
})
