import { https } from "./ConfigURL";

export let courseService = {
  getListCourse: () => {
    return https.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01");
  },
  getCategoryCourse: () => {
    return https.get("/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc")
  },
  getIdCourse: (id) => {
    return https.get(`/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${id}&MaNhom=GP01`)
  }
};
